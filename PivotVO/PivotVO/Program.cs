﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

using System.Threading;
using System.Globalization;

using ModuleUtil;
using QERPUtill;
using DBInfo;

namespace PivotVO
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            ControlCenterAddition.QERPCulture.SetCurrentThread(args);

            TBRequiredCheck[] TableCheckExisted = new TBRequiredCheck[]
            {
            };

            string LoginInfo = ModuleLoad.LoadMainOK(args, TableCheckExisted, "วิเคราะห์ข้อมูลใบสั่งจัดรถ | Vehicle Order Data Analysis");
            if (LoginInfo != "")
            {
                PivotVOForm pivotVOForm = new PivotVOForm();
                pivotVOForm.LoginInfo = LoginInfo;
                pivotVOForm.PopMessageLoadingObj = ModuleLoad.PopMessageLoadingObj;

                Application.Run(pivotVOForm);
            }
            else
            {
                Application.Exit();
            }

        }
    }
}
