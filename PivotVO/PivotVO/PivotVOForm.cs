﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using ModuleUtil;
using QEB;
using DBUtil;
using QERPUtill;
using FormatDocNo;
using PivotUtil.Data;
using DevExpress.Data.PivotGrid;

using ChartCenter;
using DevExpress.XtraCharts;
using DevExpress.Utils;
using DevExpress.XtraPivotGrid;
using DevExpress.XtraEditors;
using RightAccessOperation;
using ControlCenterAddition;
using ModuleDef;
namespace PivotVO
{
    public partial class PivotVOForm : QEB.Form
    {
        private RightAccessOperation.RightControlLevelFieldClass RightControlLevelFieldObj; 
        private PivotUtil.PivotCenter PivotCenterFunc = new PivotUtil.PivotCenter();
        private PivotFile PivotControlFile;
        public string LoginInfo = string.Empty;
        public PopMessageLoading PopMessageLoadingObj;
        private DBSimple DBSimpleFunc;
        private ModuleProgram ModuleProgramObj;
        private DataTable TBCompany;
        private DataTable TBCompanyItemUnit;
        private DataTable TBVOReport = new DataTable();
        private DataTable TBVO = new DataTable();
        private DataTable TBConfig = new DataTable();
        private string SqlVO = @"SELECT AssignVehicleOrderHeader.RunNo 
        , AssignVehicleOrderHeader.VONo
        , AssignVehicleOrderHeader.VODate 
        , AssignVehicleOrderHeader.VODate AS VODateMonth 
        , AssignVehicleOrderHeader.VODate AS VODateYear 
        , AssignVehicleOrderHeader.DispatchDate
        , AssignVehicleOrderHeader.DriverPrefix 
        , AssignVehicleOrderHeader.DriverName
        , AssignVehicleOrderHeader.DriverSuffix 
        , AssignVehicleOrderHeader.CarRegisterNo

        , AssignVehicleOrderCustomer.NumberOfMilesEnd 
        , AssignVehicleOrderCustomer.NumberOfMilesStart

        , AssignVehicleOrderHeader.EnergyExpenseBaht 
        , AssignVehicleOrderHeader.ExpressWayExpenseBaht
        , AssignVehicleOrderHeader.EnergyGasExpenseBaht 
        , AssignVehicleOrderHeader.StaffCarName
        , CASE AssignVehicleOrderHeader.DeliveryStatusComplete 
                    WHEN 'Y' THEN N'เสร็จสิ้น' 
                    WHEN 'N' THEN N'ยังไม่เสร็จ' 
                    END AS DeliveryStatusComplete

        , AssignVehicleOrderCustomer.CustomerPrefix
        , AssignVehicleOrderCustomer.CustomerName
        , AssignVehicleOrderCustomer.CustomerSuffix
        , AssignVehicleOrderDetails.Note
        , AssignVehicleOrderDetails.NodeDescription 
        , AssignVehicleOrderDetails.Quantity
        , AssignVehicleOrderDetails.RefIssuedDocumentTypeDocNo
        , CompanyItemUnits.T AS SalesUnitCodeName
        , CASE AssignVehicleOrderDetails.TransactionType 
                    WHEN 'I' THEN N'สินค้า' 
                    WHEN 'B' THEN N'ค่าใช้จ่าย' 
                    WHEN 'C' THEN N'คำอธิบาย' 
                    END AS TransactionTypeName

        , AssignVehicleOrderHeader.LastUpdated 
        , AssignVehicleOrderHeader.UpdatedUser
        , PS.T AS ProvinceState 
        , CASE WHEN(CASE WHEN ISNULL(AssignVehicleOrderDetails.QtyPerWeight,0) = 0 
                         THEN 1 ELSE ISNULL(AssignVehicleOrderDetails.QtyPerWeight,0) END 
                        / 
                        CASE WHEN ISNULL(AssignVehicleOrderDetails.Quantity,0) = 0 
                             THEN 1 ELSE ISNULL(AssignVehicleOrderDetails.Quantity,0) END) = 1 
                   THEN NULL 
                   ELSE(CASE WHEN ISNULL(AssignVehicleOrderDetails.QtyPerWeight,0) = 0 
                             THEN 1 ELSE ISNULL(AssignVehicleOrderDetails.QtyPerWeight,0) END 
                        / CASE WHEN ISNULL(AssignVehicleOrderDetails.Quantity,0) = 0 
                               THEN 1 ELSE ISNULL(AssignVehicleOrderDetails.Quantity,0) END) 
         END AS  QtyPerWeight
        , AssignVehicleOrderCustomer.CustomerActualAddress
        , AssignVehicleOrderCustomer.CustomerPostalCode
        , AssignVehicleOrderDetails.TransactionCode
        , CASE AssignVehicleOrderHeader.DeliveryStatus 
                WHEN 'N' THEN 'รอจัดส่ง'
                WHEN 'P' THEN 'กำลังจัดส่ง'
                WHEN 'Y' THEN 'ส่งสินค้าแล้ว'
                END AS DeliveryStatus
        , CM.Phone1
        , CM.Phone2
        FROM AssignVehicleOrderHeader
        LEFT JOIN AssignVehicleOrderCustomer 
        ON AssignVehicleOrderHeader.RunNo = AssignVehicleOrderCustomer.RunNo
        AND AssignVehicleOrderHeader.DocumentTypeCode = AssignVehicleOrderCustomer.DocumentTypeCode

        LEFT JOIN AssignVehicleOrderDetails
        ON AssignVehicleOrderCustomer.RunNoDocNo = AssignVehicleOrderDetails.RunNoDocNo
        AND AssignVehicleOrderHeader.RunNo = AssignVehicleOrderDetails.RunNo
        AND AssignVehicleOrderHeader.DocumentTypeCode = AssignVehicleOrderHeader.DocumentTypeCode

        LEFT JOIN CompanyItemUnits 
        ON CompanyItemUnits.Code = AssignVehicleOrderDetails.SalesUnitCode    

        LEFT JOIN QERP.dbo.Country CN
        ON AssignVehicleOrderCustomer.CustomerCountry = CN.Code
        AND CN.RecStatus = 0

        LEFT JOIN QERP.dbo.ProvinceState PS
        ON CN.Code = PS.CountryCode
        AND AssignVehicleOrderCustomer.CustomerProvinceState = PS.Code
        AND PS.RecStatus = 0

        LEFT JOIN CustomerMaster CM
        ON AssignVehicleOrderCustomer.CustomerCode = CM.Code
        AND CM.RecStatus = 0
" + " WHERE " + TABLE_MODULE.TRANS01_03VODetails + "." + FIELD_VODetails.DB_07NodeType + " = '" + VALUE_VODetails.NodeType01_TRANS + @"'";
        
        private ChartCenter.ChartClass ChartCenterFunc = new ChartClass();
        XYDiagram diagramVO { get { return this.chartControlVO.Diagram as XYDiagram; } }

        ToolTipController toolTipController = new ToolTipController();

        public PivotVOForm()
        {
            InitializeComponent();
        }

        private void PivotVOForm_Load(object sender, EventArgs e)
        {
            this.QEBSetup(this.LoginInfo);
            this.RightControlLevelFieldObj = new RightControlLevelFieldClass(new DBSimple(this.QEBConnectionQEB));
            if (this.RightControlLevelFieldObj.HasRightExecuteModuleNoLoadPKList(this.QEBCenterInfo.Username, this.QEBCenterInfo.Company
                , ModuleDef.EnumModuleName.วิเคราะห์ใบสั่งจัดรถ) == false)
            {
                this.PopMessageLoadingObj.CloseLoadingProgram();
                MessageBox.Show("ไม่มีสิทธิ์เข้าโปรแกรม");
                Application.Exit();
                return;
            }
            
            this.PopMessageLoadingObj.CloseLoadingProgram();
            this.ControlTranslator(this.QEBCenterInfo.Language);
            this.InitTableUseInForm();
            this.LoadMasterData();
            this.InitControlUseInForm();
            this.InitEvent();
            this.SetUpChartIntegration();
            this.ConfigControl();
        }

        private void ControlTranslator(string language)
        {
            switch (language)
            {
                case "T":
                    {
                        this.menuFetchData.Text = "ดึงข้อมูลล่าสุด";
                        this.menuSave.Text = "บันทึก";
                        this.menuLoad.Text = "โหลด";
                        this.menuPreview.Text = "ดูก่อนพิมพ์";
                        this.layoutTabVO.Text = "รายงานใบสั่งจัดรถ";
                        this.layoutTabReport.Text = "รายงานรายละเอียดการส่งของ";
                        this.layoutDateFrom.Text = "วันที่เริ่ม:";
                        this.layoutDateTo.Text = "วันที่ถึง:";
                        this.layoutDateFromVO.Text = "วันที่เริ่ม:";
                        this.layoutDateToVO.Text = "วันที่ถึง:";
                        this.groupControlVO.Text = "รายงาน";
                        this.radioGroup1.Properties.Items[0].Description = "รายงานรายละเอียดการส่งของ";
                    } break;

                default:
                    {
                        this.menuFetchData.Text = "Fetch Data";
                        this.menuSave.Text = "Save";
                        this.menuLoad.Text = "Load";
                        this.menuPreview.Text = "Preview";
                        this.layoutTabVO.Text = "Vehicle Order Tab";
                        this.layoutTabReport.Text = "Vehicle Order Report Details Tab";
                        this.layoutDateFrom.Text = "From:";
                        this.layoutDateTo.Text = "To:";
                        this.layoutDateFromVO.Text = "From:";
                        this.layoutDateToVO.Text = "To:";
                        this.groupControlVO.Text = "Report";
                        this.radioGroup1.Properties.Items[0].Description = "Vehicle Order Report Details";
                        this.lbVO.Text = "You can specify the view's perspective angle. the view can also be rotated via mouse and zoomed via mouse wheel. To restore default angles";
                    } break;
            }
        }

        private void InitEvent()
        {
            this.lookUpEditChartTab1.EditValueChanged += new EventHandler(lookUpEditChartTab1_EditValueChanged);
        }

        void lookUpEditChartTab1_EditValueChanged(object sender, EventArgs e)
        {
            this.ChartCenterFunc.ChangeChart(this.chartControlVO, this.lookUpEditChartTab1);
        }

        private void SetUpChartIntegration()
        {
            this.ChartCenterFunc.SetUpChartIntegration(this.chartControlVO, this.diagramVO, this.pivotGridControlVO);
            this.ChartCenterFunc.SetupChartIntegrationMouseMove(this.chartControlVO, string.Empty, string.Empty);
        }

        private void InitTableUseInForm()
        {
            this.DBSimpleFunc = new DBSimple(this.QEBConnection);

            this.QEBds = new DataSet();
            DBUtil.TableDefCreateStore TBStore = new TableDefCreateStore();
            TBStore.Add(new TableCreatedDef(TABLE_MODULE.MASTER_01CompanyItemUnits, new string[] { },true,false));
            TBStore.Add(new TableCreatedDef(TABLE_MODULE.MASTERQERP_01Company, new string[] { }, true, true));
            this.DBSimpleFunc.CreateArrayDataTable(this.QEBds, ref TBStore);

            this.TBCompany = this.QEBds.Tables[TABLE_MODULE.MASTERQERP_01Company];
            this.TBCompanyItemUnit = this.QEBds.Tables[TABLE_MODULE.MASTER_01CompanyItemUnits];
           
            this.TBVO.Columns.Add(FIELD_VOHeader.DS_01DriverFullName, System.Type.GetType("System.String"));
            this.TBVO.Columns.Add(FIELD_VOHeader.DS_02NumberOfMilesTotal, System.Type.GetType("System.Double"));
            this.TBVO.Columns.Add(FIELD_VOHeader.DS_03VirtualRunNo, System.Type.GetType("System.String"));
            this.TBVO.Columns.Add(FIELD_VOCustomer.DS_01CustomerFullName, System.Type.GetType("System.String"));

            this.pivotGridControlVO.DataSource = this.TBVO;
        }

        private void ConfigControl()
        {
            this.layoutLookUpEditChart.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
    

            foreach (DataRow rowConfig in this.TBConfig.Rows)
            {
                string FieldName = ReceiveValue.StringReceive("ConfigCode", rowConfig);
                string DefaultValue = ReceiveValue.StringReceive("Value", rowConfig);
                if (FieldName == "ChooseMultipleChartView")
                {
                    if (DefaultValue == "Y")
                        layoutLookUpEditChart.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                }
            }
        }

        private void LoadMasterData()
        {
            string sqlCompany = "SELECT * FROM " + TABLE_MODULE.MASTERQERP_01Company + " WHERE Code  = N'" + this.QEBCenterInfo.Company + "'" +
               " ORDER BY Code";
            this.DBSimpleFunc.FillData(this.TBCompany, sqlCompany, "QERP");

            string sqlCompanyItemUnit = "SELECT * FROM " + TABLE_MODULE.MASTER_01CompanyItemUnits + 
               " ORDER BY Code";
            this.DBSimpleFunc.FillData(this.TBCompanyItemUnit, sqlCompanyItemUnit);

            string sqlConfig = "SELECT * FROM CompanyConfig  WHERE CompanyCode = '" + this.QEBCenterInfo.Company + "'" +
                " AND ModuleCode = 'PivotVO' AND RecStatus = 0 ";
            this.DBSimpleFunc.FillData(this.TBConfig, sqlConfig, "QERP");
        }

        private void InitControlUseInForm()
        {
            this.ModuleProgramObj = new ModuleProgram(this.QEBConnectionQEB, this.QEBCenterInfo.Company);
            LanguageClass.setLanguage(this.QEBCenterInfo.Language);
            this.Text = this.ModuleProgramObj.GetTitleProgram(LanguageClass.ModuleName,
                this.QEBCenterInfo.Server, this.QEBCenterInfo.Company, this.QEBCenterInfo.Username);



            this.dateEditFrom.DateTime = new DateTime(DateTime.Today.Year,1,1);
            this.dateEditTo.DateTime = new DateTime(DateTime.Today.Year, 12, 31);
          
            this.InitAnchor();

            this.PivotControlFile = new PivotFile(this.pivotGridControlVO, this.DBSimpleFunc, this.QEBCenterInfo.Company);
            this.dateReportFrom.DateTime = DateTime.Now;
            this.dateReportTo.DateTime = DateTime.Now;

            ModuleCodeArgs[] ModuleArg = new ModuleCodeArgs[]
            {
                new ModuleCodeArgs(SystemDocLibrary.SALES_07AssignVehicle.DocModuleCode, EnumModuleName.ใบสั่งจัดรถ),
            };

            this.LookUpEditDocTypeCode.InitOtherControl();
            this.LookUpEditDocTypeCode.InitDocumentTypeControl(this.DBSimpleFunc, this.QEBCenterInfo, false, true, TABLE_MODULE.TRANS01_01VOHeader, ModuleArg);
            this.ChartCenterFunc.InitLookUpEditChart(this.lookUpEditChartTab1);  //Chart
           
            this.lookUpEditChartTab1.ItemIndex = 0;
        }

        private void InitAnchor()
        {
            this.pivotGridControlVO.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
            this.chartControlVO.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
            this.lbVO.Anchor = AnchorStyles.Left | AnchorStyles.Bottom;
            this.btnVO.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            
            this.lookUpEditChartTab1.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
           
            this.groupControlVO.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Top;
            this.WindowState = FormWindowState.Maximized;
            
        }

 

        void menuPreview_Click(object sender, EventArgs e)
        {
            if (!this.pivotGridControlVO.IsPrintingAvailable)
            {
                MessageBox.Show("The 'DevExpress.XtraPrinting.v7.2.dll' is not found", "Error");
                return;
            }

            this.pivotGridControlVO.ShowPrintPreview();
        }

        #region OpenFile

        

        void menuLoad_Click(object sender, EventArgs e)
        {
            this.OpenFile();
        }

        private void OpenFile()
        {
            this.Cursor = Cursors.WaitCursor;
            string NameFileOut = "";
            if (OpenFile(this.openFileDialog1, "VO.pvgrid", ref NameFileOut) == DialogResult.Cancel)
            {
                this.Cursor = Cursors.Default;
                return;
            }

            string PivotName = this.pivotGridControlVO.Name;
            string TabName = "รายงานใบสั่งจัดรถ";
            string MsgShow = string.Empty;
            try
            {
                if (!this.PivotControlFile.HasTag(NameFileOut))
                {
                    PivotFileDataSource ds = new PivotFileDataSource(NameFileOut);
                    PivotCenterFunc.CheckTemplateFile(TabName, this.pivotGridControlVO, ds, ref MsgShow);
                    if (MsgShow != string.Empty)
                    {
                        MessageBox.Show(MsgShow);
                        this.Cursor = Cursors.Default;
                        return;
                    }
                }
                else
                {
                    PivotValue CheckTab = this.PivotControlFile.GetTag(NameFileOut);
                    if (CheckTab.Key == PivotName)
                    {
                        this.PivotControlFile.LoadFile(NameFileOut);
                        this.dateEditFrom.DateTime = CheckTab.DateBegin;
                        this.dateEditTo.DateTime = CheckTab.DateEnd;
                    }
                    else
                    {
                        MessageBox.Show("ไม่สามารถเปิด File ได้ เนื่องจากชุดข้อมูลไม่ใช่ " + TabName + " กรุณาเปิด Tab " + CheckTab.TabName);
                        this.Cursor = Cursors.Default;
                        return;
                    }
                }
            }
            catch (Exception)
            {
                MessageBox.Show("ไม่สามารถเปิดไฟล์ได้");
            }
            this.Cursor = Cursors.Default;
        }

        private DialogResult OpenFile(OpenFileDialog OpenDialog, string NameIn, ref string NameOut)
        {
            DialogResult result;
            OpenDialog.FileName = NameIn;
            OpenDialog.CheckFileExists = false;
            OpenDialog.Filter = "Pivot File (.pvgrid)|*.pvgrid";
            result = OpenDialog.ShowDialog();
            NameOut = OpenDialog.FileName;
            return (result);
        }
        #endregion

        #region SaveFile
        void menuSave_Click(object sender, EventArgs e)
        {
            this.SaveFile();
        }

        private void SaveFile()
        {
            string PivotName = this.pivotGridControlVO.Name;
            string TabName = "รายงานใบสั่งจัดรถ";
            DateTime dateEditStart = this.dateEditFrom.DateTime;
            DateTime dateEditEnd = this.dateEditTo.DateTime;
            string strPivot = PivotFile.CreateTag(PivotName, dateEditStart, dateEditEnd, TabName);

            string NameFileOut = "";
            if (SaveFile(this.saveFileDialog1, "VO.pvgrid", ref NameFileOut) == DialogResult.Cancel)
                return;
            this.PivotControlFile.SaveFile(NameFileOut, strPivot);
        }

        private DialogResult SaveFile(SaveFileDialog SaveDialog, string NameIn, ref string NameOut)
        {
            DialogResult result;
            SaveDialog.FileName = NameIn;
            SaveDialog.CheckFileExists = false;
            SaveDialog.Filter = "Pivot File (.pvgrid)|*.pvgrid";
            result = SaveDialog.ShowDialog();
            NameOut = SaveDialog.FileName;
            return (result);
        }
        #endregion

        #region FetchDataVO
        void menuLoadData_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;

            DateTime DateAddDay = this.dateEditTo.DateTime.AddDays(1);
            string VODateFrom = this.dateEditFrom.DateTime.Year.ToString() + "/" + this.dateEditFrom.DateTime.Month.ToString() + "/" + this.dateEditFrom.DateTime.Day.ToString();
            string VODateTo = DateAddDay.Year.ToString() + "/" + DateAddDay.Month.ToString() + "/" + DateAddDay.Day.ToString();

            string FormatVODateFrom = this.dateEditFrom.DateTime.Day.ToString() + "/" + this.dateEditFrom.DateTime.Month.ToString() + "/" + ((this.dateEditFrom.DateTime.Year + 543).ToString());
            string FormatVODateTo = this.dateEditTo.DateTime.Day.ToString() + "/" + this.dateEditTo.DateTime.Month.ToString() + "/" + ((this.dateEditTo.DateTime.Year + 543).ToString());

            string sqlWhereVODate = " AND (" + TABLE_MODULE.TRANS01_01VOHeader + "." + FIELD_VOHeader.DB_03VODate + " >= '" + VODateFrom + "'" +
                           " AND " + TABLE_MODULE.TRANS01_01VOHeader + "." + FIELD_VOHeader.DB_03VODate + " < '" + VODateTo + "')";
            if (this.LookUpEditDocTypeCode.DocTypeCode != string.Empty && this.LookUpEditDocTypeCode.DocTypeCode != null)
                sqlWhereVODate += " AND (" + this.LookUpEditDocTypeCode.QueryDoctype + ")";

            string sqlVO = this.SqlVO + sqlWhereVODate;

            int TransErr = 0;
            string ErrMsg = string.Empty;
            int ErrCode = 0;

            this.TBVO.Clear();
            this.DBSimpleFunc.FillData(this.TBVO, sqlVO, string.Empty, ref ErrCode, ref ErrMsg);
            TransErr += this.PivotCenterFunc.ErrMsgToMessagesShow(this.QEBCenterInfo, sqlVO, ref ErrMsg);
            if (TransErr != 0)
            {
                MessageBox.Show(ErrMsg, "การดึงข้อมูลผิดพลาด");
                this.Cursor = Cursors.Default;
                return;
            }



            try
            {
                if (this.TBVO.Rows.Count == 0)
                {
                    MessageBox.Show("ไม่พบข้อมูล วิเคราะห์ข้อมูลใบสั่งจัดรถ");
                    this.Cursor = Cursors.Default;
                    return;
                }

                foreach (DataRow rowVO in this.TBVO.Rows)
                {
                    #region DriverFullName
                    string DriverPrefix = ReceiveValue.StringReceive(FIELD_VOHeader.DB_05DriverPrefix, rowVO);
                    string DriverName = ReceiveValue.StringReceive(FIELD_VOHeader.DB_06DriverName, rowVO);
                    string DriverSuffix = ReceiveValue.StringReceive(FIELD_VOHeader.DB_07DriverSuffix, rowVO);
                    string DriverFullName = DriverPrefix + DriverName + DriverSuffix;
                    #endregion

                    #region NumberOfMilesTotal
                    string NumberOfMilesStart = ReceiveValue.StringReceive(FIELD_VOHeader.DB_10NumberOfMilesStart, rowVO);
                    string NumberOfMilesEnd = ReceiveValue.StringReceive(FIELD_VOHeader.DB_09NumberOfMilesEnd, rowVO);
                    double NumberOfMilesTotal = 0.0;

                    double outNumberOfMilesStart = 0.0;
                    double outNumberOfMilesEnd = 0.0;
                    if (double.TryParse(NumberOfMilesStart, out outNumberOfMilesStart) && double.TryParse(NumberOfMilesEnd, out outNumberOfMilesEnd))
                        NumberOfMilesTotal = outNumberOfMilesEnd - outNumberOfMilesStart;

                    #endregion

                    #region VirtualRunNo
                    string VirtualRunNo = string.Empty;
                    long RunNo = ReceiveValue.LongReceive(FIELD_VOHeader.DB_01RunNo, rowVO, 0);
                    FormatDocNoFromRunningNo.FormatDocNoFromRunNo(RunNo.ToString(), ref VirtualRunNo, "");
                    #endregion

                    #region CustomerFullName
                    string CustomerPrifix = ReceiveValue.StringReceive(FIELD_VOCustomer.DB_01CustomerPrefix, rowVO);
                    string CustomerName = ReceiveValue.StringReceive(FIELD_VOCustomer.DB_02CustomerName, rowVO);
                    string CustomerSuffix = ReceiveValue.StringReceive(FIELD_VOCustomer.DB_03CustomerSuffix, rowVO);
                    string CustomerFullName = CustomerPrifix + CustomerName + CustomerSuffix;
                    #endregion

                    rowVO.BeginEdit();
                    rowVO[FIELD_VOHeader.DS_01DriverFullName] = DriverFullName;
                    rowVO[FIELD_VOHeader.DS_02NumberOfMilesTotal] = NumberOfMilesTotal;
                    rowVO[FIELD_VOHeader.DS_03VirtualRunNo] = VirtualRunNo;
                    rowVO[FIELD_VOCustomer.DS_01CustomerFullName] = CustomerFullName;
                    rowVO.EndEdit();
                }


                this.pivotGridControlVO.RefreshData();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            this.Cursor = Cursors.Default;
        }

    
        #endregion

        private void btnVO_Click(object sender, EventArgs e)
        {
            if (!this.chartControlVO.IsPrintingAvailable)
            {
                MessageBox.Show("The 'DevExpress.XtraPrinting.v7.2.dll' is not found", "Error");
                return;
            }

            this.chartControlVO.ShowPrintPreview();
        }


        private void btnPreviewReport_Click(object sender, EventArgs e)
        {
            string ErrMsg = string.Empty;
            int ErrCode = 0;
            int TransErr = 0;
            this.Cursor = Cursors.WaitCursor;

            DateTime DateAddDay = this.dateReportTo.DateTime.AddDays(1);
            string DateFrom = this.dateReportFrom.DateTime.Month.ToString() + "/" +  this.dateReportFrom.DateTime.Day.ToString() + "/"  + this.dateReportFrom.DateTime.Year.ToString();
            string DateTo = DateAddDay.Month.ToString() + "/" + DateAddDay.Day.ToString() + "/" + DateAddDay.Year.ToString();
            string sqlVOReport =
@"
        SELECT AssignVehicleOrderHeader.DriverPrefix 
        ,AssignVehicleOrderHeader.DriverName 
        ,AssignVehicleOrderHeader.DriverSuffix 
        ,AssignVehicleOrderHeader.VONo

        ,AssignVehicleOrderCustomer.CustomerPrefix 
        ,AssignVehicleOrderCustomer.CustomerName 
        ,AssignVehicleOrderCustomer.CustomerSuffix
        ,PS.T AS ProvinceState
        ,AssignVehicleOrderCustomer.CustomerActualAddress
        ,AssignVehicleOrderCustomer.CustomerPostalCode

        ,AssignVehicleOrderDetails.NodeDescription 
        ,AssignVehicleOrderDetails.Quantity 
        ,AssignVehicleOrderDetails.SalesUnitCode 
        ,AssignVehicleOrderDetails.TransactionType
        ,SalesOrderDetails.PONo
        ,AssignVehicleOrderDetails.RefIssuedDocumentTypeDocNo
        ,AssignVehicleOrderDetails.TransactionCode
        ,CASE WHEN(CASE WHEN ISNULL(AssignVehicleOrderDetails.QtyPerWeight,0) = 0 
                        THEN 1 ELSE ISNULL(AssignVehicleOrderDetails.QtyPerWeight,0) END 
                        / 
                   CASE WHEN ISNULL(AssignVehicleOrderDetails.Quantity,0) = 0 
                        THEN 1 ELSE ISNULL(AssignVehicleOrderDetails.Quantity,0) END) = 1 
              THEN NULL 
              ELSE(CASE WHEN ISNULL(AssignVehicleOrderDetails.QtyPerWeight,0) = 0 
                        THEN 1 ELSE ISNULL(AssignVehicleOrderDetails.QtyPerWeight,0) END 
                        / CASE WHEN ISNULL(AssignVehicleOrderDetails.Quantity,0) = 0 
                               THEN 1 ELSE ISNULL(AssignVehicleOrderDetails.Quantity,0) END) 
              END AS  QtyPerWeight
        FROM AssignVehicleOrderHeader 
        LEFT JOIN AssignVehicleOrderCustomer 
        ON AssignVehicleOrderHeader.RunNo = AssignVehicleOrderCustomer.RunNo 
        AND AssignVehicleOrderHeader.DocumentTypeCode = AssignVehicleOrderCustomer.DocumentTypeCode 

        LEFT JOIN AssignVehicleOrderDetails 
        ON AssignVehicleOrderCustomer.RunNoDocNo = AssignVehicleOrderDetails.RunNoDocNo 
        AND AssignVehicleOrderHeader.RunNo = AssignVehicleOrderDetails.RunNo 
        AND AssignVehicleOrderHeader.DocumentTypeCode = AssignVehicleOrderDetails.DocumentTypeCode 

        LEFT JOIN PickListDetails 
        ON PickListDetails.DocumentTypeCode = AssignVehicleOrderDetails.RefIssuedDocumentTypeCode 
        AND PickListDetails.RunNo = AssignVehicleOrderDetails.RefIssuedDocumentTypeRunNo 
        AND PickListDetails.NoLine = AssignVehicleOrderDetails.RefIssuedDocumentTypeNoLine
        AND PickListDetails.DocumentModuleCode = AssignVehicleOrderDetails.IssuedRefDocumentTypeModuleCode

        LEFT JOIN SalesOrderDetails 
        ON SalesOrderDetails.DocumentTypeCode = PickListDetails.RefIssuedDocumentTypeCode 
        AND SalesOrderDetails.RunNo = PickListDetails.RefIssuedDocumentTypeRunNo 
        AND SalesOrderDetails.NoLine = PickListDetails.RefIssuedDocumentTypeNoLine
        AND SalesOrderDetails.DocumentModuleCode = PickListDetails.IssuedRefDocumentTypeModuleCode

        LEFT JOIN QERP.dbo.Country CN
        ON AssignVehicleOrderCustomer.CustomerCountry = CN.Code
        AND CN.RecStatus = 0

        LEFT JOIN QERP.dbo.ProvinceState PS
        ON CN.Code = PS.CountryCode
        AND AssignVehicleOrderDetails.CustomerProvinceState = PS.Code
        AND AssignVehicleOrderDetails.RecStatus = 0

        WHERE AssignVehicleOrderDetails.NodeType = 'TRANS' 
AND (AssignVehicleOrderHeader.VODate >= '" + DateFrom + "' AND AssignVehicleOrderHeader.VODate < '" + DateTo + "')";
            this.DBSimpleFunc.FillData(this.TBVOReport, sqlVOReport, string.Empty, ref ErrCode, ref ErrMsg);
            TransErr += this.PivotCenterFunc.ErrMsgToMessagesShow(this.QEBCenterInfo, sqlVOReport, ref ErrMsg);
            if (TransErr != 0)
            {
                MessageBox.Show(ErrMsg, "การดึงข้อมูลผิดพลาด");
                this.Cursor = Cursors.Default;
                return;
            }


            if (this.TBVOReport.Rows.Count == 0)
            {
                MessageBox.Show("ไม่พบข้อมูล รายงานใบสั่งจัดรถ");
                this.Cursor = Cursors.Default;
                return;
            }

            IssueXtraReport AVOReport = new IssueXtraReport();
            AVODataSet AVODs = new AVODataSet();

            DataRow rowCompany = this.TBCompany.Rows[0];
            string CompanyName = ReceiveValue.StringReceive("T", rowCompany);

            #region HeaderOther

            string DateFromDisplay = this.dateReportFrom.DateTime.Day.ToString() + "/" + this.dateReportFrom.DateTime.Month.ToString() + "/" + (this.dateReportFrom.DateTime.Year + 543).ToString();
            string DateToDisplay = this.dateReportTo.DateTime.Day.ToString() + "/" + this.dateReportTo.DateTime.Month.ToString() + "/" + (this.dateReportTo.DateTime.Year + 543).ToString();

            string UserName = this.QEBCenterInfo.Username;
            if (UserName.Contains("QERP_"))
            {
                UserName = UserName.Substring(5, UserName.Length - 5);
            }

            DataTable TBDistinct = this.TBVOReport.DefaultView.ToTable(true, FIELD_VOCustomer.DB_02CustomerName);
            int CountCustomer = TBDistinct.Rows.Count;
            DataRow NewRowVOHeaderOther = AVODs.Tables["VOHeaderOther"].NewRow();
            NewRowVOHeaderOther["CompanyFullName"] = CompanyName;
            NewRowVOHeaderOther["DateFrom"] = DateFromDisplay;
            NewRowVOHeaderOther["DateTo"] = DateToDisplay;
            NewRowVOHeaderOther["CountCustomer"] = CountCustomer;
            NewRowVOHeaderOther["FooterName"] = "รายงานโดย " + UserName + " วันที่พิมพ์ " + DateTime.Now.ToString("dd/MM/yyy") + " เวลา " + DateTime.Now.ToString("hh:mm:ss") + " RPT name OP22SHIP";
            AVODs.Tables["VOHeaderOther"].Rows.Add(NewRowVOHeaderOther);
            #endregion

            #region DetailsOther
            foreach (DataRow rowDetailsOther in this.TBVOReport.Rows)
            {
                DataRow NewRowDetailsOther = AVODs.Tables["VODetailsOther"].NewRow();

                string DriverPrefix = ReceiveValue.StringReceive("DriverPrefix", rowDetailsOther);
                string DriverName = ReceiveValue.StringReceive("DriverName", rowDetailsOther);
                string DriverSuffix = ReceiveValue.StringReceive("DriverSuffix", rowDetailsOther);
                string DriverFullName = DriverPrefix + " " + DriverName + " " + DriverSuffix;
                NewRowDetailsOther["DriverFullName"] = DriverFullName;

                string CustomerPrefix = ReceiveValue.StringReceive("CustomerPrefix", rowDetailsOther);
                string CustomerName = ReceiveValue.StringReceive("CustomerName", rowDetailsOther);
                string CustomerSuffix = ReceiveValue.StringReceive("CustomerSuffix", rowDetailsOther);
                string CustomerFullName = CustomerPrefix + " " + CustomerName + " " + CustomerSuffix;
                NewRowDetailsOther["CustomerFullName"] = CustomerFullName;
                string TransType = ReceiveValue.StringReceive("TransactionType", rowDetailsOther);
                string UnitName = string.Empty;
                if (TransType == "I")
                {
                    string Unit = ReceiveValue.StringReceive(FIELD_VODetails.DB_08SalesUnitCode, rowDetailsOther);
                    DataRow[] rowArrayCompanyItemUnit = this.TBCompanyItemUnit.Select("Code = '" + Unit + "'");
                    if (rowArrayCompanyItemUnit.Length == 0)
                    {
                        MessageBox.Show("หน่วยไม่ถูกต้อง กรุณาตรวจสอบ");
                        this.Cursor = Cursors.Default;
                        return;
                    }

                    UnitName = ReceiveValue.StringReceive("T", rowArrayCompanyItemUnit[0]);
                }

                NewRowDetailsOther["DefaultUnitCode"] = UnitName;

                
                ModuleRow.CopyRow(NewRowDetailsOther, rowDetailsOther, new string[,]{
                    {"TransDesc","NodeDescription"},
                    {"Quantity","Quantity"},
                    {"RefPONo","PONo"},
                });

                AVODs.Tables["VODetailsOther"].Rows.Add(NewRowDetailsOther);
            }
            #endregion

            AVOReport.DataSource = AVODs;
            AVOReport.ShowPreviewDialog();
            this.Cursor = Cursors.Default;
        }

        private void pivotGridControlVO_MouseDown(object sender, MouseEventArgs e)
        {
            PivotGridHitInfo hitInfo = this.pivotGridControlVO.CalcHitInfo(new Point(e.X, e.Y));

            if (hitInfo.HitTest == PivotGridHitTest.None)
            {
                XtraMessageBox.Show("กรุณาเลือกที่ข้อมูลวิเคราะห์");
                this.chartControlVO.DataSource = null;
            }
            else
            {
                if (this.chartControlVO.DataSource == null)
                    this.ChartCenterFunc.SetUpChartIntegration(this.chartControlVO, this.diagramVO, this.pivotGridControlVO);
            }
        }
    }
}
