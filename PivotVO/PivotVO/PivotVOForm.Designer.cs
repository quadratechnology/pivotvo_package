﻿namespace PivotVO
{
    partial class PivotVOForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PivotVOForm));
            DevExpress.XtraCharts.XYDiagram xyDiagram1 = new DevExpress.XtraCharts.XYDiagram();
            DevExpress.XtraCharts.Series series1 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.SideBySideBarSeriesLabel sideBySideBarSeriesLabel1 = new DevExpress.XtraCharts.SideBySideBarSeriesLabel();
            DevExpress.XtraCharts.Series series2 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.SideBySideBarSeriesLabel sideBySideBarSeriesLabel2 = new DevExpress.XtraCharts.SideBySideBarSeriesLabel();
            DevExpress.XtraCharts.SideBySideBarSeriesLabel sideBySideBarSeriesLabel3 = new DevExpress.XtraCharts.SideBySideBarSeriesLabel();
            QEB.Center center1 = new QEB.Center();
            this.pivotGridControlVO = new DevExpress.XtraPivotGrid.PivotGridControl();
            this.FieldVOHeaderRunNo = new DevExpress.XtraPivotGrid.PivotGridField();
            this.FieldVOHeaderVONo = new DevExpress.XtraPivotGrid.PivotGridField();
            this.FieldVOHeaderVODate = new DevExpress.XtraPivotGrid.PivotGridField();
            this.FieldVOHeaderDispatchDate = new DevExpress.XtraPivotGrid.PivotGridField();
            this.FieldVOHeaderDriverFullName = new DevExpress.XtraPivotGrid.PivotGridField();
            this.FieldVOHeaderCarRegisterNo = new DevExpress.XtraPivotGrid.PivotGridField();
            this.FieldVOHeaderNumberOfMilesTotal = new DevExpress.XtraPivotGrid.PivotGridField();
            this.FieldVOHeaderDeliveryStatusComplete = new DevExpress.XtraPivotGrid.PivotGridField();
            this.FieldVOHeaderDeliveryStatus = new DevExpress.XtraPivotGrid.PivotGridField();
            this.FieldVOHeaderEnergyExpenseBaht = new DevExpress.XtraPivotGrid.PivotGridField();
            this.FieldVOHeaderExpressWayExpenseBaht = new DevExpress.XtraPivotGrid.PivotGridField();
            this.FieldVOCusCustomerFullName = new DevExpress.XtraPivotGrid.PivotGridField();
            this.FieldVODetailsNodeDescription = new DevExpress.XtraPivotGrid.PivotGridField();
            this.FieldVODetailsQuantity = new DevExpress.XtraPivotGrid.PivotGridField();
            this.FieldVODetailsRefInvoiceNo = new DevExpress.XtraPivotGrid.PivotGridField();
            this.VODetailsColSalesUnitCode = new DevExpress.XtraPivotGrid.PivotGridField();
            this.VODetailsColTransType = new DevExpress.XtraPivotGrid.PivotGridField();
            this.VOHeaderColEnergyGasExpenseBaht = new DevExpress.XtraPivotGrid.PivotGridField();
            this.VODetailsColNote = new DevExpress.XtraPivotGrid.PivotGridField();
            this.VOHeaderColStaffCarName = new DevExpress.XtraPivotGrid.PivotGridField();
            this.ColLastUpdated = new DevExpress.XtraPivotGrid.PivotGridField();
            this.ColUpdatedUser = new DevExpress.XtraPivotGrid.PivotGridField();
            this.FieldVODetailsTransactionCode = new DevExpress.XtraPivotGrid.PivotGridField();
            this.FieldVODetailsQtyPerWeight = new DevExpress.XtraPivotGrid.PivotGridField();
            this.FieldVOCusCustomerActualAddress = new DevExpress.XtraPivotGrid.PivotGridField();
            this.FieldVOCusCustomerPostalCode = new DevExpress.XtraPivotGrid.PivotGridField();
            this.FieldVOCusProvinceState = new DevExpress.XtraPivotGrid.PivotGridField();
            this.dateEditFrom = new DevExpress.XtraEditors.DateEdit();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.LookUpEditDocTypeCode = new ControlCenterAddition.DocumentTypeControl();
            this.btnVO = new DevExpress.XtraEditors.SimpleButton();
            this.lookUpEditChartTab1 = new DevExpress.XtraEditors.LookUpEdit();
            this.btnPreviewReport = new DevExpress.XtraEditors.SimpleButton();
            this.dateReportTo = new DevExpress.XtraEditors.DateEdit();
            this.groupControlVO = new DevExpress.XtraEditors.GroupControl();
            this.radioGroup1 = new DevExpress.XtraEditors.RadioGroup();
            this.dateReportFrom = new DevExpress.XtraEditors.DateEdit();
            this.lbVO = new DevExpress.XtraEditors.LabelControl();
            this.chartControlVO = new DevExpress.XtraCharts.ChartControl();
            this.dateEditTo = new DevExpress.XtraEditors.DateEdit();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menuFetchData = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSave = new System.Windows.Forms.ToolStripMenuItem();
            this.menuLoad = new System.Windows.Forms.ToolStripMenuItem();
            this.menuPreview = new System.Windows.Forms.ToolStripMenuItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutTabVO = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.splitterItem1 = new DevExpress.XtraLayout.SplitterItem();
            this.layoutTabReport = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.FieldVOHeaderVODateMonth = new DevExpress.XtraPivotGrid.PivotGridField();
            this.FieldVOHeaderVODateYear = new DevExpress.XtraPivotGrid.PivotGridField();
            this.layoutDateFromVO = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutPivot = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutChart = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutNote = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutDateToVO = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutPanel = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutLookUpEditChart = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutPreviewVO = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutMenu = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutDateFrom = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutGropReport = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutDateTo = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutPreview = new DevExpress.XtraLayout.LayoutControlItem();
            this.FieldVOCustomerPhone1 = new DevExpress.XtraPivotGrid.PivotGridField();
            this.FieldVOCustomerPhone2 = new DevExpress.XtraPivotGrid.PivotGridField();
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridControlVO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFrom.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFrom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditChartTab1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateReportTo.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateReportTo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlVO)).BeginInit();
            this.groupControlVO.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateReportFrom.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateReportFrom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControlVO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTo.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTo.Properties)).BeginInit();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutTabVO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutTabReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutDateFromVO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPivot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutNote)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutDateToVO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutLookUpEditChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPreviewVO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutDateFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGropReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutDateTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPreview)).BeginInit();
            this.SuspendLayout();
            // 
            // pivotGridControlVO
            // 
            this.pivotGridControlVO.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.pivotGridControlVO.Cursor = System.Windows.Forms.Cursors.Default;
            this.pivotGridControlVO.Fields.AddRange(new DevExpress.XtraPivotGrid.PivotGridField[] {
            this.FieldVOHeaderRunNo,
            this.FieldVOHeaderVONo,
            this.FieldVOHeaderVODate,
            this.FieldVOHeaderVODateMonth,
            this.FieldVOHeaderVODateYear,
            this.FieldVOHeaderDispatchDate,
            this.FieldVOHeaderDriverFullName,
            this.FieldVOHeaderCarRegisterNo,
            this.FieldVOHeaderNumberOfMilesTotal,
            this.FieldVOHeaderDeliveryStatusComplete,
            this.FieldVOHeaderDeliveryStatus,
            this.FieldVOHeaderEnergyExpenseBaht,
            this.FieldVOHeaderExpressWayExpenseBaht,
            this.FieldVOCusCustomerFullName,
            this.FieldVODetailsNodeDescription,
            this.FieldVODetailsQuantity,
            this.FieldVODetailsRefInvoiceNo,
            this.VODetailsColSalesUnitCode,
            this.VODetailsColTransType,
            this.VOHeaderColEnergyGasExpenseBaht,
            this.VODetailsColNote,
            this.VOHeaderColStaffCarName,
            this.ColLastUpdated,
            this.ColUpdatedUser,
            this.FieldVODetailsTransactionCode,
            this.FieldVODetailsQtyPerWeight,
            this.FieldVOCusCustomerActualAddress,
            this.FieldVOCusCustomerPostalCode,
            this.FieldVOCusProvinceState,
            this.FieldVOCustomerPhone1,
            this.FieldVOCustomerPhone2});
            this.pivotGridControlVO.Location = new System.Drawing.Point(14, 84);
            this.pivotGridControlVO.Name = "pivotGridControlVO";
            this.pivotGridControlVO.OptionsCustomization.AllowFilter = false;
            this.pivotGridControlVO.Size = new System.Drawing.Size(864, 261);
            this.pivotGridControlVO.TabIndex = 0;
            this.pivotGridControlVO.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pivotGridControlVO_MouseDown);
            // 
            // FieldVOHeaderRunNo
            // 
            this.FieldVOHeaderRunNo.Appearance.Header.Options.UseTextOptions = true;
            this.FieldVOHeaderRunNo.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FieldVOHeaderRunNo.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.FieldVOHeaderRunNo.AreaIndex = 0;
            this.FieldVOHeaderRunNo.Caption = "ลำดับที่";
            this.FieldVOHeaderRunNo.FieldName = "VirtualRunNo";
            this.FieldVOHeaderRunNo.Name = "FieldVOHeaderRunNo";
            this.FieldVOHeaderRunNo.Options.AllowDrag = DevExpress.Utils.DefaultBoolean.True;
            this.FieldVOHeaderRunNo.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            this.FieldVOHeaderRunNo.Options.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // FieldVOHeaderVONo
            // 
            this.FieldVOHeaderVONo.Appearance.Header.Options.UseTextOptions = true;
            this.FieldVOHeaderVONo.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FieldVOHeaderVONo.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.FieldVOHeaderVONo.AreaIndex = 1;
            this.FieldVOHeaderVONo.Caption = "เลขที่เอกสาร";
            this.FieldVOHeaderVONo.FieldName = "VONo";
            this.FieldVOHeaderVONo.Name = "FieldVOHeaderVONo";
            this.FieldVOHeaderVONo.Options.AllowDrag = DevExpress.Utils.DefaultBoolean.True;
            this.FieldVOHeaderVONo.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            this.FieldVOHeaderVONo.Options.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // FieldVOHeaderVODate
            // 
            this.FieldVOHeaderVODate.Appearance.Header.Options.UseTextOptions = true;
            this.FieldVOHeaderVODate.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FieldVOHeaderVODate.AreaIndex = 0;
            this.FieldVOHeaderVODate.Caption = "วันที่ส่งสินค้า";
            this.FieldVOHeaderVODate.FieldName = "VODate";
            this.FieldVOHeaderVODate.Name = "FieldVOHeaderVODate";
            this.FieldVOHeaderVODate.Options.AllowDrag = DevExpress.Utils.DefaultBoolean.True;
            this.FieldVOHeaderVODate.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            this.FieldVOHeaderVODate.Options.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.FieldVOHeaderVODate.ValueFormat.FormatString = "dd/MM/yyyy";
            this.FieldVOHeaderVODate.ValueFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            // 
            // FieldVOHeaderDispatchDate
            // 
            this.FieldVOHeaderDispatchDate.Appearance.Header.Options.UseTextOptions = true;
            this.FieldVOHeaderDispatchDate.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FieldVOHeaderDispatchDate.AreaIndex = 1;
            this.FieldVOHeaderDispatchDate.Caption = "วันที่สินค้าถึง";
            this.FieldVOHeaderDispatchDate.FieldName = "DispatchDate";
            this.FieldVOHeaderDispatchDate.Name = "FieldVOHeaderDispatchDate";
            this.FieldVOHeaderDispatchDate.Options.AllowDrag = DevExpress.Utils.DefaultBoolean.True;
            this.FieldVOHeaderDispatchDate.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            this.FieldVOHeaderDispatchDate.Options.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.FieldVOHeaderDispatchDate.ValueFormat.FormatString = "dd/MM/yyyy";
            this.FieldVOHeaderDispatchDate.ValueFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            // 
            // FieldVOHeaderDriverFullName
            // 
            this.FieldVOHeaderDriverFullName.Appearance.Header.Options.UseTextOptions = true;
            this.FieldVOHeaderDriverFullName.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FieldVOHeaderDriverFullName.AreaIndex = 2;
            this.FieldVOHeaderDriverFullName.Caption = "พนักงานขับรถ";
            this.FieldVOHeaderDriverFullName.FieldName = "DriverFullName";
            this.FieldVOHeaderDriverFullName.Name = "FieldVOHeaderDriverFullName";
            this.FieldVOHeaderDriverFullName.Options.AllowDrag = DevExpress.Utils.DefaultBoolean.True;
            this.FieldVOHeaderDriverFullName.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            this.FieldVOHeaderDriverFullName.Options.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // FieldVOHeaderCarRegisterNo
            // 
            this.FieldVOHeaderCarRegisterNo.Appearance.Header.Options.UseTextOptions = true;
            this.FieldVOHeaderCarRegisterNo.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FieldVOHeaderCarRegisterNo.AreaIndex = 4;
            this.FieldVOHeaderCarRegisterNo.Caption = "ทะเบียนรถ";
            this.FieldVOHeaderCarRegisterNo.FieldName = "CarRegisterNo";
            this.FieldVOHeaderCarRegisterNo.Name = "FieldVOHeaderCarRegisterNo";
            this.FieldVOHeaderCarRegisterNo.Options.AllowDrag = DevExpress.Utils.DefaultBoolean.True;
            this.FieldVOHeaderCarRegisterNo.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            this.FieldVOHeaderCarRegisterNo.Options.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // FieldVOHeaderNumberOfMilesTotal
            // 
            this.FieldVOHeaderNumberOfMilesTotal.Appearance.Header.Options.UseTextOptions = true;
            this.FieldVOHeaderNumberOfMilesTotal.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FieldVOHeaderNumberOfMilesTotal.AreaIndex = 5;
            this.FieldVOHeaderNumberOfMilesTotal.Caption = "เลขที่ไมล์";
            this.FieldVOHeaderNumberOfMilesTotal.FieldName = "NumberOfMilesTotal";
            this.FieldVOHeaderNumberOfMilesTotal.Name = "FieldVOHeaderNumberOfMilesTotal";
            this.FieldVOHeaderNumberOfMilesTotal.Options.AllowDrag = DevExpress.Utils.DefaultBoolean.True;
            this.FieldVOHeaderNumberOfMilesTotal.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            this.FieldVOHeaderNumberOfMilesTotal.Options.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // FieldVOHeaderDeliveryStatusComplete
            // 
            this.FieldVOHeaderDeliveryStatusComplete.Appearance.Header.Options.UseTextOptions = true;
            this.FieldVOHeaderDeliveryStatusComplete.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FieldVOHeaderDeliveryStatusComplete.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.FieldVOHeaderDeliveryStatusComplete.AreaIndex = 2;
            this.FieldVOHeaderDeliveryStatusComplete.Caption = "สถานะการส่งสินค้า";
            this.FieldVOHeaderDeliveryStatusComplete.FieldName = "DeliveryStatusComplete";
            this.FieldVOHeaderDeliveryStatusComplete.Name = "FieldVOHeaderDeliveryStatusComplete";
            this.FieldVOHeaderDeliveryStatusComplete.Options.AllowDrag = DevExpress.Utils.DefaultBoolean.True;
            this.FieldVOHeaderDeliveryStatusComplete.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            this.FieldVOHeaderDeliveryStatusComplete.Options.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // FieldVOHeaderDeliveryStatus
            // 
            this.FieldVOHeaderDeliveryStatus.AreaIndex = 22;
            this.FieldVOHeaderDeliveryStatus.Caption = "รายละเอียดสถานะส่งสินค้า";
            this.FieldVOHeaderDeliveryStatus.FieldName = "DeliveryStatus";
            this.FieldVOHeaderDeliveryStatus.Name = "FieldVOHeaderDeliveryStatus";
            this.FieldVOHeaderDeliveryStatus.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            this.FieldVOHeaderDeliveryStatus.Options.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // FieldVOHeaderEnergyExpenseBaht
            // 
            this.FieldVOHeaderEnergyExpenseBaht.Appearance.Header.Options.UseTextOptions = true;
            this.FieldVOHeaderEnergyExpenseBaht.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FieldVOHeaderEnergyExpenseBaht.AreaIndex = 8;
            this.FieldVOHeaderEnergyExpenseBaht.Caption = "ค่าน้ำมัน";
            this.FieldVOHeaderEnergyExpenseBaht.FieldName = "EnergyExpenseBaht";
            this.FieldVOHeaderEnergyExpenseBaht.Name = "FieldVOHeaderEnergyExpenseBaht";
            this.FieldVOHeaderEnergyExpenseBaht.Options.AllowDrag = DevExpress.Utils.DefaultBoolean.True;
            this.FieldVOHeaderEnergyExpenseBaht.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            this.FieldVOHeaderEnergyExpenseBaht.Options.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // FieldVOHeaderExpressWayExpenseBaht
            // 
            this.FieldVOHeaderExpressWayExpenseBaht.Appearance.Header.Options.UseTextOptions = true;
            this.FieldVOHeaderExpressWayExpenseBaht.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FieldVOHeaderExpressWayExpenseBaht.AreaIndex = 6;
            this.FieldVOHeaderExpressWayExpenseBaht.Caption = "ค่าทางด่วน";
            this.FieldVOHeaderExpressWayExpenseBaht.FieldName = "ExpressWayExpenseBaht";
            this.FieldVOHeaderExpressWayExpenseBaht.Name = "FieldVOHeaderExpressWayExpenseBaht";
            this.FieldVOHeaderExpressWayExpenseBaht.Options.AllowDrag = DevExpress.Utils.DefaultBoolean.True;
            this.FieldVOHeaderExpressWayExpenseBaht.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            this.FieldVOHeaderExpressWayExpenseBaht.Options.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // FieldVOCusCustomerFullName
            // 
            this.FieldVOCusCustomerFullName.Appearance.Header.Options.UseTextOptions = true;
            this.FieldVOCusCustomerFullName.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FieldVOCusCustomerFullName.AreaIndex = 10;
            this.FieldVOCusCustomerFullName.Caption = "ชื่อลูกค้า";
            this.FieldVOCusCustomerFullName.FieldName = "CustomerFullName";
            this.FieldVOCusCustomerFullName.Name = "FieldVOCusCustomerFullName";
            this.FieldVOCusCustomerFullName.Options.AllowDrag = DevExpress.Utils.DefaultBoolean.True;
            this.FieldVOCusCustomerFullName.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            this.FieldVOCusCustomerFullName.Options.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // FieldVODetailsNodeDescription
            // 
            this.FieldVODetailsNodeDescription.Appearance.Header.Options.UseTextOptions = true;
            this.FieldVODetailsNodeDescription.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FieldVODetailsNodeDescription.AreaIndex = 11;
            this.FieldVODetailsNodeDescription.Caption = "รายการ";
            this.FieldVODetailsNodeDescription.FieldName = "NodeDescription";
            this.FieldVODetailsNodeDescription.Name = "FieldVODetailsNodeDescription";
            this.FieldVODetailsNodeDescription.Options.AllowDrag = DevExpress.Utils.DefaultBoolean.True;
            this.FieldVODetailsNodeDescription.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            this.FieldVODetailsNodeDescription.Options.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // FieldVODetailsQuantity
            // 
            this.FieldVODetailsQuantity.Appearance.Header.Options.UseTextOptions = true;
            this.FieldVODetailsQuantity.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FieldVODetailsQuantity.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea;
            this.FieldVODetailsQuantity.AreaIndex = 0;
            this.FieldVODetailsQuantity.Caption = "จำนวน";
            this.FieldVODetailsQuantity.FieldName = "Quantity";
            this.FieldVODetailsQuantity.Name = "FieldVODetailsQuantity";
            this.FieldVODetailsQuantity.Options.AllowDrag = DevExpress.Utils.DefaultBoolean.True;
            this.FieldVODetailsQuantity.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            this.FieldVODetailsQuantity.Options.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // FieldVODetailsRefInvoiceNo
            // 
            this.FieldVODetailsRefInvoiceNo.Appearance.Header.Options.UseTextOptions = true;
            this.FieldVODetailsRefInvoiceNo.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FieldVODetailsRefInvoiceNo.AreaIndex = 9;
            this.FieldVODetailsRefInvoiceNo.Caption = "เลขที่เอกสารอ้างอิง";
            this.FieldVODetailsRefInvoiceNo.FieldName = "RefIssuedDocumentTypeDocNo";
            this.FieldVODetailsRefInvoiceNo.Name = "FieldVODetailsRefInvoiceNo";
            this.FieldVODetailsRefInvoiceNo.Options.AllowDrag = DevExpress.Utils.DefaultBoolean.True;
            this.FieldVODetailsRefInvoiceNo.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            this.FieldVODetailsRefInvoiceNo.Options.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // VODetailsColSalesUnitCode
            // 
            this.VODetailsColSalesUnitCode.AreaIndex = 12;
            this.VODetailsColSalesUnitCode.Caption = "หน่วย";
            this.VODetailsColSalesUnitCode.FieldName = "SalesUnitCodeName";
            this.VODetailsColSalesUnitCode.Name = "VODetailsColSalesUnitCode";
            this.VODetailsColSalesUnitCode.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            this.VODetailsColSalesUnitCode.Options.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // VODetailsColTransType
            // 
            this.VODetailsColTransType.AreaIndex = 13;
            this.VODetailsColTransType.Caption = "ประเภท";
            this.VODetailsColTransType.FieldName = "TransactionTypeName";
            this.VODetailsColTransType.Name = "VODetailsColTransType";
            this.VODetailsColTransType.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            this.VODetailsColTransType.Options.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // VOHeaderColEnergyGasExpenseBaht
            // 
            this.VOHeaderColEnergyGasExpenseBaht.AreaIndex = 7;
            this.VOHeaderColEnergyGasExpenseBaht.Caption = "ค่าแก๊ส";
            this.VOHeaderColEnergyGasExpenseBaht.FieldName = "EnergyGasExpenseBaht";
            this.VOHeaderColEnergyGasExpenseBaht.Name = "VOHeaderColEnergyGasExpenseBaht";
            this.VOHeaderColEnergyGasExpenseBaht.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            this.VOHeaderColEnergyGasExpenseBaht.Options.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // VODetailsColNote
            // 
            this.VODetailsColNote.AreaIndex = 14;
            this.VODetailsColNote.Caption = "หมายเหตุ";
            this.VODetailsColNote.FieldName = "Note";
            this.VODetailsColNote.Name = "VODetailsColNote";
            this.VODetailsColNote.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            this.VODetailsColNote.Options.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // VOHeaderColStaffCarName
            // 
            this.VOHeaderColStaffCarName.AreaIndex = 3;
            this.VOHeaderColStaffCarName.Caption = "พนักงานติดรถ";
            this.VOHeaderColStaffCarName.FieldName = "StaffCarName";
            this.VOHeaderColStaffCarName.Name = "VOHeaderColStaffCarName";
            this.VOHeaderColStaffCarName.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            this.VOHeaderColStaffCarName.Options.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // ColLastUpdated
            // 
            this.ColLastUpdated.AreaIndex = 15;
            this.ColLastUpdated.Caption = "วันที่แก้ไขล่าสุด";
            this.ColLastUpdated.CellFormat.FormatString = "dd/MM/yyy";
            this.ColLastUpdated.CellFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.ColLastUpdated.FieldName = "LastUpdated";
            this.ColLastUpdated.Name = "ColLastUpdated";
            this.ColLastUpdated.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            this.ColLastUpdated.Options.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.ColLastUpdated.ValueFormat.FormatString = "dd/MM/yyy";
            this.ColLastUpdated.ValueFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            // 
            // ColUpdatedUser
            // 
            this.ColUpdatedUser.AreaIndex = 16;
            this.ColUpdatedUser.Caption = "ผู้ใช้ล่าสุด";
            this.ColUpdatedUser.FieldName = "UpdatedUser";
            this.ColUpdatedUser.Name = "ColUpdatedUser";
            this.ColUpdatedUser.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            this.ColUpdatedUser.Options.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // FieldVODetailsTransactionCode
            // 
            this.FieldVODetailsTransactionCode.AreaIndex = 17;
            this.FieldVODetailsTransactionCode.Caption = "รหัสสินค้า";
            this.FieldVODetailsTransactionCode.FieldName = "TransactionCode";
            this.FieldVODetailsTransactionCode.Name = "FieldVODetailsTransactionCode";
            this.FieldVODetailsTransactionCode.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            this.FieldVODetailsTransactionCode.Options.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // FieldVODetailsQtyPerWeight
            // 
            this.FieldVODetailsQtyPerWeight.AreaIndex = 19;
            this.FieldVODetailsQtyPerWeight.Caption = "น้ำหนักต่อหน่วย";
            this.FieldVODetailsQtyPerWeight.FieldName = "QtyPerWeight";
            this.FieldVODetailsQtyPerWeight.Name = "FieldVODetailsQtyPerWeight";
            this.FieldVODetailsQtyPerWeight.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            this.FieldVODetailsQtyPerWeight.Options.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // FieldVOCusCustomerActualAddress
            // 
            this.FieldVOCusCustomerActualAddress.AreaIndex = 18;
            this.FieldVOCusCustomerActualAddress.Caption = "สถานที่จัดส่ง";
            this.FieldVOCusCustomerActualAddress.FieldName = "CustomerActualAddress";
            this.FieldVOCusCustomerActualAddress.Name = "FieldVOCusCustomerActualAddress";
            this.FieldVOCusCustomerActualAddress.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            this.FieldVOCusCustomerActualAddress.Options.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // FieldVOCusCustomerPostalCode
            // 
            this.FieldVOCusCustomerPostalCode.AreaIndex = 21;
            this.FieldVOCusCustomerPostalCode.Caption = "รหัสไปรษณีย์";
            this.FieldVOCusCustomerPostalCode.FieldName = "CustomerPostalCode";
            this.FieldVOCusCustomerPostalCode.Name = "FieldVOCusCustomerPostalCode";
            this.FieldVOCusCustomerPostalCode.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            this.FieldVOCusCustomerPostalCode.Options.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // FieldVOCusProvinceState
            // 
            this.FieldVOCusProvinceState.AreaIndex = 20;
            this.FieldVOCusProvinceState.Caption = "จังหวัด";
            this.FieldVOCusProvinceState.FieldName = "ProvinceState";
            this.FieldVOCusProvinceState.Name = "FieldVOCusProvinceState";
            this.FieldVOCusProvinceState.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            this.FieldVOCusProvinceState.Options.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // dateEditFrom
            // 
            this.dateEditFrom.EditValue = null;
            this.dateEditFrom.Location = new System.Drawing.Point(59, 60);
            this.dateEditFrom.Name = "dateEditFrom";
            this.dateEditFrom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditFrom.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditFrom.Size = new System.Drawing.Size(115, 20);
            this.dateEditFrom.StyleController = this.layoutControl1;
            this.dateEditFrom.TabIndex = 1;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.panelControl1);
            this.layoutControl1.Controls.Add(this.btnVO);
            this.layoutControl1.Controls.Add(this.lookUpEditChartTab1);
            this.layoutControl1.Controls.Add(this.btnPreviewReport);
            this.layoutControl1.Controls.Add(this.dateReportTo);
            this.layoutControl1.Controls.Add(this.groupControlVO);
            this.layoutControl1.Controls.Add(this.dateReportFrom);
            this.layoutControl1.Controls.Add(this.pivotGridControlVO);
            this.layoutControl1.Controls.Add(this.lbVO);
            this.layoutControl1.Controls.Add(this.dateEditFrom);
            this.layoutControl1.Controls.Add(this.chartControlVO);
            this.layoutControl1.Controls.Add(this.dateEditTo);
            this.layoutControl1.Controls.Add(this.menuStrip1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(892, 566);
            this.layoutControl1.TabIndex = 39;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.LookUpEditDocTypeCode);
            this.panelControl1.Location = new System.Drawing.Point(340, 60);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(538, 20);
            this.panelControl1.TabIndex = 48;
            // 
            // LookUpEditDocTypeCode
            // 
            this.LookUpEditDocTypeCode.Location = new System.Drawing.Point(5, 0);
            this.LookUpEditDocTypeCode.Name = "LookUpEditDocTypeCode";
            this.LookUpEditDocTypeCode.Size = new System.Drawing.Size(315, 20);
            this.LookUpEditDocTypeCode.TabIndex = 128;
            // 
            // btnVO
            // 
            this.btnVO.Image = ((System.Drawing.Image)(resources.GetObject("btnVO.Image")));
            this.btnVO.Location = new System.Drawing.Point(791, 513);
            this.btnVO.Name = "btnVO";
            this.btnVO.Size = new System.Drawing.Size(87, 38);
            this.btnVO.StyleController = this.layoutControl1;
            this.btnVO.TabIndex = 36;
            this.btnVO.Text = "Preview";
            this.btnVO.Click += new System.EventHandler(this.btnVO_Click);
            // 
            // lookUpEditChartTab1
            // 
            this.lookUpEditChartTab1.Location = new System.Drawing.Point(648, 513);
            this.lookUpEditChartTab1.Name = "lookUpEditChartTab1";
            this.lookUpEditChartTab1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEditChartTab1.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Desc", "Desc")});
            this.lookUpEditChartTab1.Properties.NullText = "";
            this.lookUpEditChartTab1.Size = new System.Drawing.Size(139, 20);
            this.lookUpEditChartTab1.StyleController = this.layoutControl1;
            this.lookUpEditChartTab1.TabIndex = 38;
            // 
            // btnPreviewReport
            // 
            this.btnPreviewReport.Image = ((System.Drawing.Image)(resources.GetObject("btnPreviewReport.Image")));
            this.btnPreviewReport.Location = new System.Drawing.Point(402, 70);
            this.btnPreviewReport.Name = "btnPreviewReport";
            this.btnPreviewReport.Size = new System.Drawing.Size(119, 38);
            this.btnPreviewReport.StyleController = this.layoutControl1;
            this.btnPreviewReport.TabIndex = 45;
            this.btnPreviewReport.Text = "Preview";
            this.btnPreviewReport.Click += new System.EventHandler(this.btnPreviewReport_Click);
            // 
            // dateReportTo
            // 
            this.dateReportTo.EditValue = null;
            this.dateReportTo.Location = new System.Drawing.Point(200, 36);
            this.dateReportTo.Name = "dateReportTo";
            this.dateReportTo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateReportTo.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateReportTo.Size = new System.Drawing.Size(86, 20);
            this.dateReportTo.StyleController = this.layoutControl1;
            this.dateReportTo.TabIndex = 47;
            // 
            // groupControlVO
            // 
            this.groupControlVO.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.groupControlVO.Controls.Add(this.radioGroup1);
            this.groupControlVO.Location = new System.Drawing.Point(14, 70);
            this.groupControlVO.Name = "groupControlVO";
            this.groupControlVO.Size = new System.Drawing.Size(384, 482);
            this.groupControlVO.TabIndex = 44;
            this.groupControlVO.Text = "รายงาน";
            // 
            // radioGroup1
            // 
            this.radioGroup1.Location = new System.Drawing.Point(5, 25);
            this.radioGroup1.Name = "radioGroup1";
            this.radioGroup1.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(null, "รายงานรายละเอียดการส่งของ")});
            this.radioGroup1.Size = new System.Drawing.Size(326, 38);
            this.radioGroup1.TabIndex = 39;
            // 
            // dateReportFrom
            // 
            this.dateReportFrom.EditValue = null;
            this.dateReportFrom.Location = new System.Drawing.Point(59, 36);
            this.dateReportFrom.Name = "dateReportFrom";
            this.dateReportFrom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateReportFrom.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateReportFrom.Size = new System.Drawing.Size(97, 20);
            this.dateReportFrom.StyleController = this.layoutControl1;
            this.dateReportFrom.TabIndex = 46;
            // 
            // lbVO
            // 
            this.lbVO.Location = new System.Drawing.Point(14, 513);
            this.lbVO.Name = "lbVO";
            this.lbVO.Size = new System.Drawing.Size(600, 39);
            this.lbVO.StyleController = this.layoutControl1;
            this.lbVO.TabIndex = 10;
            this.lbVO.Text = resources.GetString("lbVO.Text");
            // 
            // chartControlVO
            // 
            this.chartControlVO.AppearanceName = "In A Fog";
            xyDiagram1.AxisX.Range.ScrollingRange.SideMarginsEnabled = true;
            xyDiagram1.AxisX.Range.SideMarginsEnabled = true;
            xyDiagram1.AxisX.VisibleInPanesSerializable = "-1";
            xyDiagram1.AxisY.Range.ScrollingRange.SideMarginsEnabled = true;
            xyDiagram1.AxisY.Range.SideMarginsEnabled = true;
            xyDiagram1.AxisY.VisibleInPanesSerializable = "-1";
            xyDiagram1.EnableScrolling = true;
            xyDiagram1.EnableZooming = true;
            this.chartControlVO.Diagram = xyDiagram1;
            this.chartControlVO.Location = new System.Drawing.Point(14, 355);
            this.chartControlVO.Name = "chartControlVO";
            sideBySideBarSeriesLabel1.LineVisible = true;
            series1.Label = sideBySideBarSeriesLabel1;
            series1.Name = "Series 1";
            sideBySideBarSeriesLabel2.LineVisible = true;
            series2.Label = sideBySideBarSeriesLabel2;
            series2.Name = "Series 2";
            this.chartControlVO.SeriesSerializable = new DevExpress.XtraCharts.Series[] {
        series1,
        series2};
            sideBySideBarSeriesLabel3.LineVisible = true;
            this.chartControlVO.SeriesTemplate.Label = sideBySideBarSeriesLabel3;
            this.chartControlVO.Size = new System.Drawing.Size(864, 154);
            this.chartControlVO.TabIndex = 6;
            // 
            // dateEditTo
            // 
            this.dateEditTo.EditValue = null;
            this.dateEditTo.Location = new System.Drawing.Point(218, 60);
            this.dateEditTo.Name = "dateEditTo";
            this.dateEditTo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditTo.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dateEditTo.Size = new System.Drawing.Size(118, 20);
            this.dateEditTo.StyleController = this.layoutControl1;
            this.dateEditTo.TabIndex = 2;
            // 
            // menuStrip1
            // 
            this.menuStrip1.AutoSize = false;
            this.menuStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuFetchData,
            this.menuSave,
            this.menuLoad,
            this.menuPreview});
            this.menuStrip1.Location = new System.Drawing.Point(14, 36);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(864, 20);
            this.menuStrip1.TabIndex = 49;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // menuFetchData
            // 
            this.menuFetchData.Name = "menuFetchData";
            this.menuFetchData.Size = new System.Drawing.Size(81, 16);
            this.menuFetchData.Text = "ดึงข้อมูลล่าสุด";
            this.menuFetchData.Click += new System.EventHandler(this.menuLoadData_Click);
            // 
            // menuSave
            // 
            this.menuSave.Name = "menuSave";
            this.menuSave.Size = new System.Drawing.Size(43, 16);
            this.menuSave.Text = "Save";
            this.menuSave.Click += new System.EventHandler(this.menuSave_Click);
            // 
            // menuLoad
            // 
            this.menuLoad.Name = "menuLoad";
            this.menuLoad.Size = new System.Drawing.Size(45, 16);
            this.menuLoad.Text = "Load";
            this.menuLoad.Click += new System.EventHandler(this.menuLoad_Click);
            // 
            // menuPreview
            // 
            this.menuPreview.Name = "menuPreview";
            this.menuPreview.Size = new System.Drawing.Size(60, 16);
            this.menuPreview.Text = "Preview";
            this.menuPreview.Click += new System.EventHandler(this.menuPreview_Click);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.tabbedControlGroup1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(892, 566);
            this.layoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.CustomizationFormText = "tabbedControlGroup1";
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutTabVO;
            this.tabbedControlGroup1.SelectedTabPageIndex = 0;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(892, 566);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutTabVO,
            this.layoutTabReport});
            this.tabbedControlGroup1.Text = "tabbedControlGroup1";
            // 
            // layoutTabVO
            // 
            this.layoutTabVO.CustomizationFormText = "รายงานใบสั่งจัดรถ";
            this.layoutTabVO.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutDateFromVO,
            this.layoutPivot,
            this.layoutChart,
            this.layoutNote,
            this.layoutDateToVO,
            this.layoutPanel,
            this.layoutLookUpEditChart,
            this.layoutPreviewVO,
            this.emptySpaceItem1,
            this.splitterItem1,
            this.layoutMenu});
            this.layoutTabVO.Location = new System.Drawing.Point(0, 0);
            this.layoutTabVO.Name = "layoutTabVO";
            this.layoutTabVO.Size = new System.Drawing.Size(868, 520);
            this.layoutTabVO.Text = "รายงานใบสั่งจัดรถ";
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(604, 477);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(30, 43);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // splitterItem1
            // 
            this.splitterItem1.AllowHotTrack = true;
            this.splitterItem1.CustomizationFormText = "splitterItem1";
            this.splitterItem1.Location = new System.Drawing.Point(0, 313);
            this.splitterItem1.Name = "splitterItem1";
            this.splitterItem1.Size = new System.Drawing.Size(868, 6);
            // 
            // layoutTabReport
            // 
            this.layoutTabReport.CustomizationFormText = "รายงานรายละเอียดการส่งของ";
            this.layoutTabReport.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutDateFrom,
            this.layoutGropReport,
            this.layoutDateTo,
            this.layoutPreview,
            this.emptySpaceItem3,
            this.emptySpaceItem4,
            this.emptySpaceItem5,
            this.emptySpaceItem2});
            this.layoutTabReport.Location = new System.Drawing.Point(0, 0);
            this.layoutTabReport.Name = "layoutTabReport";
            this.layoutTabReport.Size = new System.Drawing.Size(868, 520);
            this.layoutTabReport.Text = "รายงานรายละเอียดการส่งของ";
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(388, 76);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(480, 444);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(276, 0);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(592, 24);
            this.emptySpaceItem4.Text = "emptySpaceItem4";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 24);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(868, 10);
            this.emptySpaceItem5.Text = "emptySpaceItem5";
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(511, 34);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(357, 42);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // FieldVOHeaderVODateMonth
            // 
            this.FieldVOHeaderVODateMonth.Appearance.Header.Options.UseTextOptions = true;
            this.FieldVOHeaderVODateMonth.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FieldVOHeaderVODateMonth.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.FieldVOHeaderVODateMonth.AreaIndex = 0;
            this.FieldVOHeaderVODateMonth.Caption = "เดือน";
            this.FieldVOHeaderVODateMonth.FieldName = "VODateMonth";
            this.FieldVOHeaderVODateMonth.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateMonth;
            this.FieldVOHeaderVODateMonth.Name = "FieldVOHeaderVODateMonth";
            this.FieldVOHeaderVODateMonth.Options.AllowDrag = DevExpress.Utils.DefaultBoolean.True;
            this.FieldVOHeaderVODateMonth.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            this.FieldVOHeaderVODateMonth.Options.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.FieldVOHeaderVODateMonth.UnboundFieldName = "FieldVOHeaderVODateMonth";
            // 
            // FieldVOHeaderVODateYear
            // 
            this.FieldVOHeaderVODateYear.Appearance.Header.Options.UseTextOptions = true;
            this.FieldVOHeaderVODateYear.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FieldVOHeaderVODateYear.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea;
            this.FieldVOHeaderVODateYear.AreaIndex = 1;
            this.FieldVOHeaderVODateYear.Caption = "ปี";
            this.FieldVOHeaderVODateYear.FieldName = "VODateYear";
            this.FieldVOHeaderVODateYear.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.DateYear;
            this.FieldVOHeaderVODateYear.Name = "FieldVOHeaderVODateYear";
            this.FieldVOHeaderVODateYear.Options.AllowDrag = DevExpress.Utils.DefaultBoolean.True;
            this.FieldVOHeaderVODateYear.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            this.FieldVOHeaderVODateYear.Options.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.FieldVOHeaderVODateYear.UnboundFieldName = "FieldVOHeaderVODateYear";
            // 
            // layoutDateFromVO
            // 
            this.layoutDateFromVO.Control = this.dateEditFrom;
            this.layoutDateFromVO.CustomizationFormText = "layoutControlItem1";
            this.layoutDateFromVO.Location = new System.Drawing.Point(0, 24);
            this.layoutDateFromVO.MaxSize = new System.Drawing.Size(164, 24);
            this.layoutDateFromVO.MinSize = new System.Drawing.Size(164, 24);
            this.layoutDateFromVO.Name = "layoutDateFromVO";
            this.layoutDateFromVO.Size = new System.Drawing.Size(164, 24);
            this.layoutDateFromVO.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutDateFromVO.Text = "วันที่เริ่ม:";
            this.layoutDateFromVO.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutDateFromVO.TextSize = new System.Drawing.Size(40, 13);
            this.layoutDateFromVO.TextToControlDistance = 5;
            // 
            // layoutPivot
            // 
            this.layoutPivot.Control = this.pivotGridControlVO;
            this.layoutPivot.CustomizationFormText = "layoutPivot";
            this.layoutPivot.Location = new System.Drawing.Point(0, 48);
            this.layoutPivot.Name = "layoutPivot";
            this.layoutPivot.Size = new System.Drawing.Size(868, 265);
            this.layoutPivot.Text = "layoutPivot";
            this.layoutPivot.TextSize = new System.Drawing.Size(0, 0);
            this.layoutPivot.TextToControlDistance = 0;
            this.layoutPivot.TextVisible = false;
            // 
            // layoutChart
            // 
            this.layoutChart.Control = this.chartControlVO;
            this.layoutChart.CustomizationFormText = "layoutChart";
            this.layoutChart.Location = new System.Drawing.Point(0, 319);
            this.layoutChart.Name = "layoutChart";
            this.layoutChart.Size = new System.Drawing.Size(868, 158);
            this.layoutChart.Text = "layoutChart";
            this.layoutChart.TextSize = new System.Drawing.Size(0, 0);
            this.layoutChart.TextToControlDistance = 0;
            this.layoutChart.TextVisible = false;
            // 
            // layoutNote
            // 
            this.layoutNote.Control = this.lbVO;
            this.layoutNote.CustomizationFormText = "layoutNote";
            this.layoutNote.Location = new System.Drawing.Point(0, 477);
            this.layoutNote.Name = "layoutNote";
            this.layoutNote.Size = new System.Drawing.Size(604, 43);
            this.layoutNote.Text = "layoutNote";
            this.layoutNote.TextSize = new System.Drawing.Size(0, 0);
            this.layoutNote.TextToControlDistance = 0;
            this.layoutNote.TextVisible = false;
            // 
            // layoutDateToVO
            // 
            this.layoutDateToVO.Control = this.dateEditTo;
            this.layoutDateToVO.CustomizationFormText = "วันที่ถึง:";
            this.layoutDateToVO.Location = new System.Drawing.Point(164, 24);
            this.layoutDateToVO.MaxSize = new System.Drawing.Size(162, 24);
            this.layoutDateToVO.MinSize = new System.Drawing.Size(162, 24);
            this.layoutDateToVO.Name = "layoutDateToVO";
            this.layoutDateToVO.Size = new System.Drawing.Size(162, 24);
            this.layoutDateToVO.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutDateToVO.Text = "วันที่ถึง:";
            this.layoutDateToVO.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutDateToVO.TextSize = new System.Drawing.Size(35, 13);
            this.layoutDateToVO.TextToControlDistance = 5;
            // 
            // layoutPanel
            // 
            this.layoutPanel.Control = this.panelControl1;
            this.layoutPanel.CustomizationFormText = "layoutPanel";
            this.layoutPanel.Location = new System.Drawing.Point(326, 24);
            this.layoutPanel.Name = "layoutPanel";
            this.layoutPanel.Size = new System.Drawing.Size(542, 24);
            this.layoutPanel.Text = "layoutPanel";
            this.layoutPanel.TextSize = new System.Drawing.Size(0, 0);
            this.layoutPanel.TextToControlDistance = 0;
            this.layoutPanel.TextVisible = false;
            // 
            // layoutLookUpEditChart
            // 
            this.layoutLookUpEditChart.Control = this.lookUpEditChartTab1;
            this.layoutLookUpEditChart.CustomizationFormText = "layoutLookUpEditChart";
            this.layoutLookUpEditChart.Location = new System.Drawing.Point(634, 477);
            this.layoutLookUpEditChart.MaxSize = new System.Drawing.Size(143, 24);
            this.layoutLookUpEditChart.MinSize = new System.Drawing.Size(143, 24);
            this.layoutLookUpEditChart.Name = "layoutLookUpEditChart";
            this.layoutLookUpEditChart.Size = new System.Drawing.Size(143, 43);
            this.layoutLookUpEditChart.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutLookUpEditChart.Text = "layoutLookUpEditChart";
            this.layoutLookUpEditChart.TextSize = new System.Drawing.Size(0, 0);
            this.layoutLookUpEditChart.TextToControlDistance = 0;
            this.layoutLookUpEditChart.TextVisible = false;
            // 
            // layoutPreviewVO
            // 
            this.layoutPreviewVO.Control = this.btnVO;
            this.layoutPreviewVO.CustomizationFormText = "layoutControlItem8";
            this.layoutPreviewVO.Location = new System.Drawing.Point(777, 477);
            this.layoutPreviewVO.MaxSize = new System.Drawing.Size(91, 42);
            this.layoutPreviewVO.MinSize = new System.Drawing.Size(91, 42);
            this.layoutPreviewVO.Name = "layoutPreviewVO";
            this.layoutPreviewVO.Size = new System.Drawing.Size(91, 43);
            this.layoutPreviewVO.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutPreviewVO.Text = "layoutPreviewVO";
            this.layoutPreviewVO.TextSize = new System.Drawing.Size(0, 0);
            this.layoutPreviewVO.TextToControlDistance = 0;
            this.layoutPreviewVO.TextVisible = false;
            // 
            // layoutMenu
            // 
            this.layoutMenu.Control = this.menuStrip1;
            this.layoutMenu.CustomizationFormText = "layoutMenu";
            this.layoutMenu.Location = new System.Drawing.Point(0, 0);
            this.layoutMenu.Name = "layoutMenu";
            this.layoutMenu.Size = new System.Drawing.Size(868, 24);
            this.layoutMenu.Text = "layoutMenu";
            this.layoutMenu.TextSize = new System.Drawing.Size(0, 0);
            this.layoutMenu.TextToControlDistance = 0;
            this.layoutMenu.TextVisible = false;
            // 
            // layoutDateFrom
            // 
            this.layoutDateFrom.Control = this.dateReportFrom;
            this.layoutDateFrom.CustomizationFormText = "วันที่เริ่ม:";
            this.layoutDateFrom.Location = new System.Drawing.Point(0, 0);
            this.layoutDateFrom.Name = "layoutDateFrom";
            this.layoutDateFrom.Size = new System.Drawing.Size(146, 24);
            this.layoutDateFrom.Text = "วันที่เริ่ม:";
            this.layoutDateFrom.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutDateFrom.TextSize = new System.Drawing.Size(40, 13);
            this.layoutDateFrom.TextToControlDistance = 5;
            // 
            // layoutGropReport
            // 
            this.layoutGropReport.Control = this.groupControlVO;
            this.layoutGropReport.CustomizationFormText = "layoutGropReport";
            this.layoutGropReport.Location = new System.Drawing.Point(0, 34);
            this.layoutGropReport.MaxSize = new System.Drawing.Size(388, 0);
            this.layoutGropReport.MinSize = new System.Drawing.Size(388, 24);
            this.layoutGropReport.Name = "layoutGropReport";
            this.layoutGropReport.Size = new System.Drawing.Size(388, 486);
            this.layoutGropReport.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutGropReport.Text = "layoutGropReport";
            this.layoutGropReport.TextSize = new System.Drawing.Size(0, 0);
            this.layoutGropReport.TextToControlDistance = 0;
            this.layoutGropReport.TextVisible = false;
            // 
            // layoutDateTo
            // 
            this.layoutDateTo.Control = this.dateReportTo;
            this.layoutDateTo.CustomizationFormText = "วันที่ถึง:";
            this.layoutDateTo.Location = new System.Drawing.Point(146, 0);
            this.layoutDateTo.Name = "layoutDateTo";
            this.layoutDateTo.Size = new System.Drawing.Size(130, 24);
            this.layoutDateTo.Text = "วันที่ถึง:";
            this.layoutDateTo.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutDateTo.TextSize = new System.Drawing.Size(35, 13);
            this.layoutDateTo.TextToControlDistance = 5;
            // 
            // layoutPreview
            // 
            this.layoutPreview.Control = this.btnPreviewReport;
            this.layoutPreview.CustomizationFormText = "layoutPreview";
            this.layoutPreview.Location = new System.Drawing.Point(388, 34);
            this.layoutPreview.MaxSize = new System.Drawing.Size(123, 42);
            this.layoutPreview.MinSize = new System.Drawing.Size(123, 42);
            this.layoutPreview.Name = "layoutPreview";
            this.layoutPreview.Size = new System.Drawing.Size(123, 42);
            this.layoutPreview.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutPreview.Text = "layoutPreview";
            this.layoutPreview.TextSize = new System.Drawing.Size(0, 0);
            this.layoutPreview.TextToControlDistance = 0;
            this.layoutPreview.TextVisible = false;
            // 
            // FieldVOCustomerPhone1
            // 
            this.FieldVOCustomerPhone1.AreaIndex = 23;
            this.FieldVOCustomerPhone1.Caption = "โทรศัพท์ลูกค้า 1";
            this.FieldVOCustomerPhone1.FieldName = "Phone1";
            this.FieldVOCustomerPhone1.Name = "FieldVOCustomerPhone1";
            this.FieldVOCustomerPhone1.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            this.FieldVOCustomerPhone1.Options.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // FieldVOCustomerPhone2
            // 
            this.FieldVOCustomerPhone2.AreaIndex = 24;
            this.FieldVOCustomerPhone2.Caption = "โทรศัพท์ลูกค้า 2";
            this.FieldVOCustomerPhone2.FieldName = "Phone2";
            this.FieldVOCustomerPhone2.Name = "FieldVOCustomerPhone2";
            this.FieldVOCustomerPhone2.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            this.FieldVOCustomerPhone2.Options.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // PivotVOForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(892, 566);
            this.Controls.Add(this.layoutControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "PivotVOForm";
            center1.Company = "";
            center1.ErrorOn = true;
            center1.Password = null;
            center1.Server = null;
            center1.Username = null;
            this.QEBCenterInfo = center1;
            this.Text = "วิเคราะห์ข้อมูลใบสั่งจัดรถ";
            this.Load += new System.EventHandler(this.PivotVOForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridControlVO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFrom.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditFrom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditChartTab1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateReportTo.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateReportTo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlVO)).EndInit();
            this.groupControlVO.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radioGroup1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateReportFrom.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateReportFrom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartControlVO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTo.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditTo.Properties)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutTabVO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutTabReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutDateFromVO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPivot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutNote)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutDateToVO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutLookUpEditChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPreviewVO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutDateFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutGropReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutDateTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPreview)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraPivotGrid.PivotGridControl pivotGridControlVO;
        private DevExpress.XtraEditors.DateEdit dateEditFrom;
        private DevExpress.XtraEditors.DateEdit dateEditTo;
        private DevExpress.XtraPivotGrid.PivotGridField FieldVOHeaderRunNo;
        private DevExpress.XtraPivotGrid.PivotGridField FieldVOHeaderVONo;
        private DevExpress.XtraPivotGrid.PivotGridField FieldVOHeaderVODate;
        private DevExpress.XtraPivotGrid.PivotGridField FieldVOHeaderVODateMonth;
        private DevExpress.XtraPivotGrid.PivotGridField FieldVOHeaderVODateYear;
        private DevExpress.XtraPivotGrid.PivotGridField FieldVOHeaderDispatchDate;
        private DevExpress.XtraPivotGrid.PivotGridField FieldVOHeaderDriverFullName;
        private DevExpress.XtraPivotGrid.PivotGridField FieldVOHeaderCarRegisterNo;
        private DevExpress.XtraPivotGrid.PivotGridField FieldVOHeaderNumberOfMilesTotal;
        private DevExpress.XtraPivotGrid.PivotGridField FieldVOHeaderDeliveryStatusComplete;
        private DevExpress.XtraPivotGrid.PivotGridField FieldVOHeaderEnergyExpenseBaht;
        private DevExpress.XtraPivotGrid.PivotGridField FieldVOHeaderExpressWayExpenseBaht;
        private DevExpress.XtraPivotGrid.PivotGridField FieldVOCusCustomerFullName;
        private DevExpress.XtraPivotGrid.PivotGridField FieldVODetailsNodeDescription;
        private DevExpress.XtraPivotGrid.PivotGridField FieldVODetailsQuantity;
        private DevExpress.XtraPivotGrid.PivotGridField FieldVODetailsRefInvoiceNo;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private DevExpress.XtraCharts.ChartControl chartControlVO;
        private DevExpress.XtraEditors.LabelControl lbVO;
        private DevExpress.XtraEditors.SimpleButton btnVO;
        private DevExpress.XtraEditors.SimpleButton btnPreviewReport;
        private DevExpress.XtraEditors.GroupControl groupControlVO;
        private DevExpress.XtraEditors.RadioGroup radioGroup1;
        private DevExpress.XtraEditors.DateEdit dateReportFrom;
        private DevExpress.XtraEditors.DateEdit dateReportTo;
        private DevExpress.XtraPivotGrid.PivotGridField VODetailsColSalesUnitCode;
        private DevExpress.XtraPivotGrid.PivotGridField VODetailsColTransType;
        private ControlCenterAddition.DocumentTypeControl LookUpEditDocTypeCode;
        private DevExpress.XtraEditors.LookUpEdit lookUpEditChartTab1;
        private DevExpress.XtraPivotGrid.PivotGridField VOHeaderColEnergyGasExpenseBaht;
        private DevExpress.XtraPivotGrid.PivotGridField VODetailsColNote;
        private DevExpress.XtraPivotGrid.PivotGridField VOHeaderColStaffCarName;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutTabReport;
        private DevExpress.XtraLayout.LayoutControlItem layoutDateFrom;
        private DevExpress.XtraLayout.LayoutControlItem layoutDateTo;
        private DevExpress.XtraLayout.LayoutControlItem layoutGropReport;
        private DevExpress.XtraLayout.LayoutControlItem layoutPreview;
        private DevExpress.XtraLayout.LayoutControlGroup layoutTabVO;
        private DevExpress.XtraLayout.LayoutControlItem layoutDateFromVO;
        private DevExpress.XtraLayout.LayoutControlItem layoutDateToVO;
        private DevExpress.XtraLayout.LayoutControlItem layoutChart;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutNote;
        private DevExpress.XtraLayout.LayoutControlItem layoutLookUpEditChart;
        private DevExpress.XtraLayout.LayoutControlItem layoutPreviewVO;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraLayout.LayoutControlItem layoutPanel;
        private DevExpress.XtraLayout.SplitterItem splitterItem1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menuFetchData;
        private System.Windows.Forms.ToolStripMenuItem menuSave;
        private System.Windows.Forms.ToolStripMenuItem menuLoad;
        private System.Windows.Forms.ToolStripMenuItem menuPreview;
        private DevExpress.XtraLayout.LayoutControlItem layoutMenu;
        private DevExpress.XtraPivotGrid.PivotGridField ColLastUpdated;
        private DevExpress.XtraPivotGrid.PivotGridField ColUpdatedUser;
        private DevExpress.XtraPivotGrid.PivotGridField FieldVODetailsTransactionCode;
        private DevExpress.XtraPivotGrid.PivotGridField FieldVODetailsQtyPerWeight;
        private DevExpress.XtraPivotGrid.PivotGridField FieldVOCusCustomerActualAddress;
        private DevExpress.XtraPivotGrid.PivotGridField FieldVOCusCustomerPostalCode;
        private DevExpress.XtraPivotGrid.PivotGridField FieldVOCusProvinceState;
        private DevExpress.XtraLayout.LayoutControlItem layoutPivot;
        private DevExpress.XtraPivotGrid.PivotGridField FieldVOHeaderDeliveryStatus;
        private DevExpress.XtraPivotGrid.PivotGridField FieldVOCustomerPhone1;
        private DevExpress.XtraPivotGrid.PivotGridField FieldVOCustomerPhone2;
    }
}

