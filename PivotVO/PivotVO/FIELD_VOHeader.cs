﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PivotVO
{
    class FIELD_VOHeader
    {
        public const string DB_01RunNo = "RunNo";
        public const string DB_02VONo = "VONo";
        public const string DB_03VODate = "VODate";
        public const string DB_04DispatchDate = "DispatchDate";
        public const string DB_05DriverPrefix = "DriverPrefix";
        public const string DB_06DriverName = "DriverName";
        public const string DB_07DriverSuffix = "DriverSuffix";
        public const string DB_08CarRegisterNo = "CarRegisterNo";
        public const string DB_09NumberOfMilesEnd = "NumberOfMilesEnd";
        public const string DB_10NumberOfMilesStart = "NumberOfMilesStart";
        public const string DB_11EnergyExpenseBaht = "EnergyExpenseBaht";
        public const string DB_12ExpressWayExpenseBaht = "ExpressWayExpenseBaht";
        public const string DB_13DeliveryStatusComplete = "DeliveryStatusComplete";
        public const string DB_14DomesticExport = "DomesticExport";


        public const string DS_01DriverFullName = "DriverFullName";
        public const string DS_02NumberOfMilesTotal = "NumberOfMilesTotal";
        public const string DS_03VirtualRunNo = "VirtualRunNo";
    }
}
