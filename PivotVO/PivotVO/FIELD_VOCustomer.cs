﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PivotVO
{
    class FIELD_VOCustomer
    {
        public const string DB_01CustomerPrefix = "CustomerPrefix";
        public const string DB_02CustomerName = "CustomerName";
        public const string DB_03CustomerSuffix = "CustomerSuffix";
        public const string DB_04RunNo = "RunNo";
        public const string DB_05DomesticExport = "DomesticExport";
        public const string DB_06RunNoDocNo = "RunNoDocNo";

        public const string DS_01CustomerFullName = "CustomerFullName";
    }
}
