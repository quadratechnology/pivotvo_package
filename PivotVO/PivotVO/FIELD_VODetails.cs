﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PivotVO
{
    class FIELD_VODetails
    {
        public const string DB_01NodeDescription = "NodeDescription";
        public const string DB_02Quantity = "Quantity";
        public const string DB_03RefIssuedDocumentTypeDocNo = "RefIssuedDocumentTypeDocNo";
        public const string DB_04RunNoDocNo = "RunNoDocNo";
        public const string DB_05RunNo = "RunNo";
        public const string DB_06DomesticExport = "DomesticExport";
        public const string DB_07NodeType = "NodeType";
        public const string DB_08SalesUnitCode = "SalesUnitCode";
        public const string DB_09TransactionCode = "TransactionCode";
        public const string DB_10QtyPerWeight = "QtyPerWeight";
    }
}
