﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PivotVO
{
    class TABLE_MODULE
    {
        public const string TRANS01_01VOHeader = "AssignVehicleOrderHeader";
        public const string TRANS01_02VOCustomer = "AssignVehicleOrderCustomer";
        public const string TRANS01_03VODetails = "AssignVehicleOrderDetails";

        public const string MASTER_01CompanyItemUnits = "CompanyItemUnits";

        public const string MASTERQERP_01Company = "Company";
    }
}
