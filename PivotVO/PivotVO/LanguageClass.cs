﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PivotVO
{
    public static class LanguageClass
    {
        public static string ModuleName = "วิเคราะห์ข้อมูลใบสั่งจัดรถ | Vehicle Order Data Analysis";
        public static void setLanguage(string language)
        {
            ModuleName = language == "T" ? "วิเคราะห์ข้อมูลใบสั่งจัดรถ" : "Vehicle Order Data Analysis";
        }
    }
}
